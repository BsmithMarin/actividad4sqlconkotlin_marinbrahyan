package tablasBBDD

class MaquinasVsEmpleados {

    var idMaquinasVsEmpleados:Int? =null
    var idEmpleado:Int? = null
    var idMaquina:Int? = null

    constructor(IDmaquinaVSempleado:Int,IDempleado:Int,IDmaquina:Int){
        idMaquinasVsEmpleados = IDmaquinaVSempleado
        idEmpleado = IDempleado
        idMaquina = IDmaquina
    }

}