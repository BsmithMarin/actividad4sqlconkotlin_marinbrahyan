package tablasBBDD

class Producto {

    var idProducto:Int? = null
    var Nombre:String? = null
    var tipoIva:Double? = null
    var caducidadMedia:Int? = null

    constructor(id:Int,nombre:String,Iva:Double,CaducidadDias:Int){

        idProducto = id
        Nombre = nombre
        tipoIva = Iva
        caducidadMedia= CaducidadDias

    }

}