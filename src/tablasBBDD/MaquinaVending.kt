package tablasBBDD

import java.sql.Date

class MaquinaVending {

    var idMaquina:Int? = null
    var ciudad:String? = null
    var direccion:String? = null
    var codigoPostal:Int? = null
    var fechaAdquisicion:Date? = null

    constructor(IDMaquina:Int,Ciudad:String,Direccion:String,CodigoPostal:Int,FechaCompra:Date){

        idMaquina=IDMaquina
        ciudad=Ciudad
        direccion=Direccion
        codigoPostal=CodigoPostal
        fechaAdquisicion=FechaCompra
    }

}