package tablasBBDD

class MaquinasVSProductos {

    var idMvp:Int? = null
    var idMaquina:Int? = null
    var idProducto:Int? = null
    var precioVenta:Double? = null

    constructor(IDMVP:Int,IDmaquina:Int,IDproducto:Int,PrecioVenta:Double){

        idMvp=IDMVP
        idMaquina=IDmaquina
        idProducto=IDproducto
        precioVenta=PrecioVenta
    }

}