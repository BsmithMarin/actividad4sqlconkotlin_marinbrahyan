package tablasBBDD

import java.sql.Date

class PedididoProveedores {

    var idPedido:Int?=null
    var idPvP:Int?=null
    var cantidad:Int?=null
    var precioUd:Double?=null
    var formaDePago:String?=null
    var fecha:Date?=null
    var totalFactura:Double?=null

    constructor(IDPedido:Int,IDPvP:Int,Cantidad:Int,PrecioUd:Double,FormaDePago:String,Fecha:Date,TotalFactura:Double){

        idPedido=IDPedido
        idPvP=IDPvP
        cantidad=Cantidad
        precioUd=PrecioUd
        formaDePago=FormaDePago
        fecha=Fecha
        totalFactura=TotalFactura
    }
}
