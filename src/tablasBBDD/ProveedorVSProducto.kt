package tablasBBDD

class ProveedorVSProducto {

    var idPvP:Int? = null
    var idProducto:Int? = null
    var idProveedor:Int? = null
    var precio:Double? = null
    var plazoEntrega:Int?= null

    constructor(IDPvP:Int,IDProducto:Int,IDProveedor:Int,Precio:Double,PlazoEntrega:Int){

        idPvP=IDPvP
        idProducto=IDProducto
        idProveedor=IDProveedor
        precio=Precio
        plazoEntrega=PlazoEntrega

    }
}