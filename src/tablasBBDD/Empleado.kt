package tablasBBDD



class Empleado {

    var idEmpleado:Int?=null
    var Nombre:String?=null
    var NIF:String?=null
    var Ciudad:String?=null

    constructor(id:Int,nombre:String,DNI:String,ciudad:String){

        idEmpleado=id
        Nombre=nombre
        NIF=DNI
        Ciudad=ciudad
    }
}