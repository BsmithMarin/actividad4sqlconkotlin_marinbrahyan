package tablasBBDD

class Proveedor {

    var idProveedor:Int? = null
    var nombre:String? = null
    var Nif:String? = null
    var nTelefono: Int? = null
    var ciudad:String? = null

    constructor(IDProveedor:Int,Nombre:String,NIF:String,NTelefono:Int,Ciudad:String){

        idProveedor=IDProveedor
        nombre=Nombre
        Nif=NIF
        nTelefono=NTelefono
        ciudad=Ciudad

    }

}