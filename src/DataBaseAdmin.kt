import tablasBBDD.Empleado
import tablasBBDD.Producto
import tablasBBDD.MaquinasVsEmpleados
import tablasBBDD.MaquinasVSProductos
import tablasBBDD.MaquinaVending
import tablasBBDD.PedididoProveedores
import tablasBBDD.Proveedor
import tablasBBDD.ProveedorVSProducto
import tablasBBDD.Reposicion

import java.lang.Exception
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement


class DataBaseAdmin {

    var conn:Connection?=null

    constructor(){

    }

    //Crea la conexion con la base datos

    fun iniciarConexion(){

        val strUrld:String = "jdbc:postgresql://rogue.db.elephantsql.com:5432/gnfpqxfv"
        val strUser:String = "gnfpqxfv"
        val strPassword:String = "oZvN5W1y0M_fcArFjoEvjodiZW-DP9v6"



        try {

            conn=DriverManager.getConnection(strUrld,strUser,strPassword)

            println("Conexion establecida")

        }catch (e:Exception){

            e.printStackTrace() //Imprime errores sin detener el programa

        }
    }

    /*
    A partir de aqui se definen todas las funciones utilizadas para la lectura de las diferentes tablas
    que componen la base de datos, todas siguen la misma estructura, realizan una consulta a la base de datos
    y partir de la devolucion guardan las filas de la tabla en un objeto, que luego se incluirá en una lista que
    tendá todas las filas en forma de objetos.
     */

    fun leerTablaProductos():ArrayList<Producto>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaProductos= arrayListOf<Producto>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"Productos\" ")

            while (resultSet.next()){
                var Producto: Producto = Producto((resultSet.getInt("IDProducto")) , (resultSet.getString("Nombre")),
                                                (resultSet.getDouble("TipoIva")) , (resultSet.getInt("CaducidadMediaDias")))
                arListaProductos.add(Producto)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaProductos

    }

    fun leerTablaEmpleados():ArrayList<Empleado>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaEmpleados= arrayListOf<Empleado>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"Empleados\" ")

            while (resultSet.next()){
                var Empleado: Empleado = Empleado((resultSet.getInt("IDEmpleado")) , (resultSet.getString("Nombre")),
                        (resultSet.getString("NIF")) , (resultSet.getString("Ciudad")))
                arListaEmpleados.add(Empleado)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaEmpleados

    }

    fun leerTablaMaquinasVSEmpleados():ArrayList<MaquinasVsEmpleados>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaMaquinasVsEmpleados= arrayListOf<MaquinasVsEmpleados>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"MaquinasVSEmpleados\"")

            while (resultSet.next()){
                var MaquinaVsEmpleado: MaquinasVsEmpleados = MaquinasVsEmpleados((resultSet.getInt("IDMvE")) , (resultSet.getInt("IDEmpleado")),
                        (resultSet.getInt("IDMaquina")))
                arListaMaquinasVsEmpleados.add(MaquinaVsEmpleado)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaMaquinasVsEmpleados

    }

    fun leerTablaMaquinasVSProductos():ArrayList<MaquinasVSProductos>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaMaquinasVSProductos= arrayListOf<MaquinasVSProductos>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"MaquinasVSProductos\"")

            while (resultSet.next()){
                var MaquinaVSProducto: MaquinasVSProductos = MaquinasVSProductos((resultSet.getInt("IDMvP")) , (resultSet.getInt("IDMaquina")),
                        (resultSet.getInt("IDProducto")) , (resultSet.getDouble("PrecioVenta")))
                arListaMaquinasVSProductos.add(MaquinaVSProducto)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaMaquinasVSProductos

    }

    fun leerTablaMaquinasVending():ArrayList<MaquinaVending>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaMaquinasVending= arrayListOf<MaquinaVending>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"MaquinasVending\"")

            while (resultSet.next()){
                var MaquinaVending: MaquinaVending = MaquinaVending((resultSet.getInt("IDMaquina")) , (resultSet.getString("Ciudad")),
                        (resultSet.getString("Direccion")) , (resultSet.getInt("CodigoPostal")),(resultSet.getDate("FechaAdquisicion")))
                arListaMaquinasVending.add(MaquinaVending)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaMaquinasVending

    }

    fun leerTablaPedidosProveedores():ArrayList<PedididoProveedores>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaPedididosProveedores= arrayListOf<PedididoProveedores>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"PedidosProveedores\"")

            while (resultSet.next()){
                var PedididoProveedor: PedididoProveedores = PedididoProveedores((resultSet.getInt("IDPedido")) , (resultSet.getInt("IDPvP")),
                        (resultSet.getInt("Cantidad")) , (resultSet.getDouble("PrecioUd")),(resultSet.getString("FormaDePago")),
                        (resultSet.getDate("Fecha")),(resultSet.getDouble("TotalFactura")))
                arListaPedididosProveedores.add(PedididoProveedor)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaPedididosProveedores

    }

    fun leerTablaProveedores():ArrayList<Proveedor>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaProveedores= arrayListOf<Proveedor>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"Proveedores\"")

            while (resultSet.next()){
                var Proveedor: Proveedor = Proveedor((resultSet.getInt(1)) , (resultSet.getString(2)),
                        (resultSet.getString(3 )) , (resultSet.getInt(4)),(resultSet.getString(5)))
                arListaProveedores.add(Proveedor)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaProveedores

    }

    fun leerTablaProveedoresVSProductos():ArrayList<ProveedorVSProducto>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaProveedoresVSProductos= arrayListOf<ProveedorVSProducto>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"ProveedoresVSProductos\"")

            while (resultSet.next()){
                var ProveedorVSProducto: ProveedorVSProducto = ProveedorVSProducto((resultSet.getInt(1)) , (resultSet.getInt(2)),
                        (resultSet.getInt(3 )) , (resultSet.getDouble(4)),(resultSet.getInt(5)))
                arListaProveedoresVSProductos.add(ProveedorVSProducto)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaProveedoresVSProductos

    }

    fun leerTablaReposiciones():ArrayList<Reposicion>{

        var statement:Statement? = null
        var resultSet: ResultSet? = null
        var arListaReposiciones= arrayListOf<Reposicion>()

        try {

            statement = conn!!.createStatement()
            resultSet = statement.executeQuery("SELECT * FROM \"public\".\"Reposiciones\"")

            while (resultSet.next()){
                var Reposicion: Reposicion = Reposicion((resultSet.getInt(1)) , (resultSet.getDate(2)),
                        (resultSet.getInt(3 )) , (resultSet.getInt(4)))
                arListaReposiciones.add(Reposicion)
            }
        }catch (e:Exception){

            e.printStackTrace()
        }

        return  arListaReposiciones

    }



}