
/*
Se crea la conexion con a base de datos, ademas se comprueba que la conexion funciona
imprimeinto por consola alguna de las columnas de cada tabla

Se a creado un paquete dentro de la carpeta src, en la que se almacenan todas las clases
utilizadas para guardar los datos de las tablas
 */

fun main(){
    var dataBaseAdmin:DataBaseAdmin=DataBaseAdmin()
    dataBaseAdmin.iniciarConexion()

    for(i in dataBaseAdmin.leerTablaProductos()){
        print(i.Nombre + " ")
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaEmpleados()){
        print(i.Nombre + " ")
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaMaquinasVSEmpleados()){
        print(" "+i.idMaquinasVsEmpleados )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaMaquinasVSProductos()){
        print(" "+i.precioVenta )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaMaquinasVending()){
        print(" "+i.fechaAdquisicion )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaPedidosProveedores()){
        print(" "+i.fecha )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaProveedores()){
        print(" "+i.nombre )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaProveedoresVSProductos()){
        print(" "+i.plazoEntrega )
    }

    println(" \n ")
    for(i in dataBaseAdmin.leerTablaReposiciones()){
        print(" "+i.fecha )
    }

}